<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeUserController extends Controller
{
    public function withNickname($name, $nickname){
    	$name =ucfirst($name);
		return "Bienvenido {$name}, tu apodo es {$nickname}";		
    }
	
	public function withoutNickname($name){
		return "Bienvenido {$name}";
	}

}
