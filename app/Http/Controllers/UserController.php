<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){

    	$users =['jose','juan','pedro', 'luis', 'maria'];

    	return view('users',[ 
    		'users' => $users,
    		'title' => 'Listado de Usuarios'

    		 ]);
    }

	public function show($id){
		return view('showUser',[ 
    		'id' => $id,
    		'title' => 'Detalle de Usuario'

    		 ]);
    }

    public function create(){
    	return view('createUser',[
    		'title' => 'Crear Usuario'
    	]);
    }
}
